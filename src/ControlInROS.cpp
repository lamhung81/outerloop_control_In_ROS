#include "ControlInROS.h"

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>

#include <sstream>
#include <string>
#include <time.h>
#include <algorithm>
#include <ros/ros.h>
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
//#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Imu.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
#include <opencv2/highgui/highgui.hpp>
#include <rosbag/bag.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64MultiArray.h>
//#include "algorithmhom.h"
//#include <thread>  //c++ 11 ?

#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/Float64.h>

#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

#include <libconfig.h++>
#include <libconfig.h>

#include <ros/callback_queue.h>



#include <../eigen3/Eigen/Core>
#include <../eigen3/Eigen/Dense>



using namespace std;
using namespace message_filters;

using namespace cv;
using namespace libconfig;
using namespace sensor_msgs;
using namespace Eigen;


std::vector<float> ratex_vector(0.0);
std::vector<float> ratey_vector(0.0);
std::vector<float> ratez_vector(0.0);

/*
VectorXf ratex_vector_new(5);
VectorXf ratey_vector_new(5);
VectorXf ratez_vector_new(5);
*/

VectorXf ratex_vector_new(10);
VectorXf ratey_vector_new(10);
VectorXf ratez_vector_new(10);

/*
typedef Matrix<float, 5, 1> Vector5f;


Vector5f ratex_vector_new(0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
Vector5f ratey_vector_new(0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
Vector5f ratez_vector_new(0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
*/


int counter = 0;



//Run online
bool _isManualMode = true;
bool _isStartHomogCalcul = false;



/*
//Run offline test with rosbag and without joystick
bool _isManualMode = false; //true;
bool _isStartHomogCalcul = true; // false;
*/


bool _isEmmergencyStop = false;
int _LampButtonStatus = 0;
int _PressureButtonStatus = 0;
float _Fgb_3 = 2.0;
float _usePixhawkIMU = false; //True if use Pixhawk's IMU
                             //False if use Camera's IMU

bool _isStartedIntegrator = false;


//float _Fc_bar1 = 0.0;
//float _Fc_bar2 = 0.0;
//float _Fc_bar3 = 0.0;




ControlInROS::ControlInROS(): ratex(0.0),ratey(0.0),ratez(0.0), _z_ep(0.0, 0.0, 0.0), _ep_hat(0.0, 0.0, 0.0), _omega_3r(0.0){

	//Timestamps
	gyrotimestamp  = 0.0;

    if (_usePixhawkIMU == true){
        gyroSub    = nh.subscribe("/mavros/imu/data_raw",1000,&ControlInROS::gyroCallback,this); //Use Pixhawk camera
    }else{
        gyroSub    = nh.subscribe("/imu/data_raw",1000,&ControlInROS::gyroCallback,this); //Use IMU of the camera
    }

    //gyroSub    = nh.subscribe("/mavros/imu/data_raw",1000,&ControlInROS::gyroCallback,this); //Use Pixhawk camera
    //gyroSub    = nh.subscribe("/mavros/imu/data_raw",1,&ControlInROS::gyroCallback,this);



    joySub = nh.subscribe("/joy", 10, &ControlInROS::joyCallback, this);

    //homoSub = nh.subscribe("/imghom", 1000, &ControlInROS::homoCallback,this);
    homoSub = nh.subscribe("/homography", 1000, &ControlInROS::homoCallback,this);

    angVelPub = nh.advertise<geometry_msgs::TwistStamped>("/mavros/setpoint_attitude/cmd_vel",10);
    throttlePub = nh.advertise<std_msgs::Float64>("/mavros/setpoint_attitude/att_throttle",10);
    quatPub = nh.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_attitude/attitude",10);

    accelPub = nh.advertise<geometry_msgs::Vector3Stamped>("/mavros/setpoint_accel/accel",10);
    positionPub = nh.advertise<geometry_msgs::PoseStamped>("/mavros/setpoint_position/local",10);
    velPub = nh.advertise<geometry_msgs::TwistStamped>("/mavros/setpoint_velocity/cmd_vel",10);
    
    zepPub = nh.advertise<geometry_msgs::Vector3Stamped>("/zep",10);
    epPub = nh.advertise<geometry_msgs::Vector3Stamped>("/ep",10);
    epHatPub = nh.advertise<geometry_msgs::Vector3Stamped>("/epHat",10);
    FcontrolPub = nh.advertise<geometry_msgs::Vector3Stamped>("/Fc",10);

}


void ControlInROS::homoCallback(const i3s_img_proc::hom::ConstPtr &homodata){
    //New mapping
    //First collumn
    _h11 = homodata ->homography[0];
    _h21 = homodata ->homography[1];
    _h31 = homodata ->homography[2];
    //Second collumn
    _h12 = homodata ->homography[3];
    _h22 = homodata ->homography[4];
    _h32 = homodata ->homography[5];
    //Third collumn
    _h13 = homodata ->homography[6];
    _h23 = homodata ->homography[7];
    _h33 = homodata ->homography[8];



}


void ControlInROS::gyroCallback(const sensor_msgs::Imu::ConstPtr &gyrodata){

    if (_usePixhawkIMU = true){
        //If use Pixhawk IMU
        //Converting from Pixhawk IMU reference frame to AUV's frame
        ratex = gyrodata->angular_velocity.x;
        ratey = gyrodata->angular_velocity.y; ratey = -1.0*ratey;
        ratez = gyrodata->angular_velocity.z; ratez = -1.0*ratez;
    }else{
        //If use Camera IMU : Downward-looking camera
        //Converting from Camera IMU reference frame to AUV's frame
        ratex =       gyrodata->angular_velocity.z;
        ratey = -1.0*(gyrodata->angular_velocity.y);
        ratez =       gyrodata->angular_velocity.x;


//        //If use Camera IMU : Forward-looking camera
//        //Converting from Camera IMU reference frame to AUV's frame
//        ratex =       gyrodata->angular_velocity.x;
//        ratey = -1.0*(gyrodata->angular_velocity.y);
//        ratez = -1.0*(gyrodata->angular_velocity.z);

    }

	//Timestamp of the gyro readings
	gyrotimestamp = gyrodata->header.stamp.sec;

	ratex_vector.push_back(ratex);
	ratey_vector.push_back(ratey);
	ratez_vector.push_back(ratez);

	ratex_vector_new[counter] = ratex;
	ratey_vector_new[counter] = ratey;
	ratez_vector_new[counter] = ratez;

        //ROS_INFO("Debug gyroCallback angVelo: % 1.6f % 1.6f % 1.6f %1d", ratex, ratey, ratez, counter);

	counter ++;
	if (counter >= 10){
		counter = 0;
	}
}


void ControlInROS::joyCallback(const sensor_msgs::Joy::ConstPtr &joy){

    //Logic of state machine
    int PowerButton = 0;
    PowerButton = joy -> buttons[8];
    if (PowerButton > 0.5){
        _isEmmergencyStop = true;
    }

    if(_isEmmergencyStop){
        ROS_INFO("Emmergency stop both (Manual, Autonomous) control modes and Homography calculation");
    }
    else{

        //Read state of start button: for start Homography calculation
        int StartButton = 0;
        StartButton = joy -> buttons[7];
        if ((StartButton > 0.5)||(_h11 > 0.5)){  //_h11 for initilizing when launchingi3s_img_proc online_Homo_observer_DPC
            _isStartHomogCalcul = true;
        }

        //Read state of back button: for stop Homography calculation
        int BackButton = 0;
        BackButton = joy -> buttons[6];
        if (BackButton == 1){
            _isStartHomogCalcul = false;
        }

        //Read state of A button: for Autonomous control mode
        int AButton = 0;
        AButton = joy -> buttons[0];

        if (AButton > 0.5){
            if (_isStartHomogCalcul){
                ROS_INFO("Homography calculation has been initilized");
                _isManualMode = false;  //Means that is in Autonomous control mode
            }
            else{
                ROS_INFO("Homography calculation has not been initilized");
                ROS_INFO("Need to initilize homography ca_h11lculation first");
            }

        }

        //Read state of Y button: for Manual control mode
        int YButton = 0;
        YButton = joy -> buttons[3];

        if (YButton > 0.5){
            _isManualMode = true;
        }


        if(_isManualMode){
            ROS_INFO("In Manual control mode");
            if (_isStartHomogCalcul){
                ROS_INFO("Homography calculation has been initilized");
            }
            else{
                ROS_INFO("No Homography calculation");
            }
        }
        else{
            ROS_INFO("In Autonomous control mode");
        }

        int LampButtonCheck = 0;
        LampButtonCheck = joy -> axes[7] ;

        if (LampButtonCheck > 0) {
            _LampButtonStatus = 1;
        }else if (LampButtonCheck < 0)
        {
             _LampButtonStatus = -1;
        }else{
             _LampButtonStatus = 0;
        }

        int PressureButtonCheck = 0;
        PressureButtonCheck = joy -> axes[6] ;

        if (PressureButtonCheck > 0) {
            _PressureButtonStatus = 1;
        }else if (PressureButtonCheck < 0)
        {
             _PressureButtonStatus = -1;
        }else{
             _PressureButtonStatus = 0;
        }

    }

    //End of //Logic of state machine


    if (_isEmmergencyStop){
        geometry_msgs::TwistStamped TwistStampedMsg;
        TwistStampedMsg.twist.angular.x = 1.0; //joy -> buttons[8];
        //TwistStampedMsg.twist.angular.y = joy -> buttons[8];
        //TwistStampedMsg.twist.angular.z = joy -> buttons[8];
        angVelPub.publish(TwistStampedMsg);
        ROS_INFO("Debug Emmergency: % 1.6f  ", TwistStampedMsg.twist.angular.x);

        //Need 3 following lines in order not stuck at emmergency stop
        //lhnguyen: do not understand why need this?
        //Need to figure out!f
        std_msgs::Float64 throttleMsg;
        throttleMsg.data = joy->axes[1];// ROS_INFO("throttle received: [%f]", throttleMsg.data);
        throttlePub.publish(throttleMsg);

    } else {
        if (_isManualMode){

            _isStartedIntegrator = false;

            //Info about Manual control mode
            geometry_msgs::TwistStamped TwistStampedMsg;
            TwistStampedMsg.twist.angular.y = 1.0;
            //TwistStampedMsg.twist.angular.z = 1.0;  //joy->axes[2];

            if (_LampButtonStatus > 0) {
                TwistStampedMsg.twist.angular.z = 1.0;
            }else if (_LampButtonStatus < 0)
            {
                TwistStampedMsg.twist.angular.z = -1.0;
            }else{
                TwistStampedMsg.twist.angular.z = 0.0;
            }
            ROS_INFO("Debug Lamp in Manual: % 1.6f  ", TwistStampedMsg.twist.angular.z);
            angVelPub.publish(TwistStampedMsg);
            //ROS_INFO("Debug In Manual Mode: % 1.6f  ", TwistStampedMsg.twist.angular.y);

            //Send orientation and Yaw velocity
            geometry_msgs::Vector3Stamped Vector3StampedMsg;
            Vector3StampedMsg.vector.x =  joy->axes[3]; //0.5; //  ROS_INFO("roll ang.vel received: [TwistStampedMsg%f]" , TwistStampedMsg.twist.angular.x);
            Vector3StampedMsg.vector.y =  joy->axes[4]; //0.6; // ROS_INFO("pitch ang.vel received: [%f]", TwistStampedMsg.twist.angular.y);
            Vector3StampedMsg.vector.z =  joy->axes[0]; //0.7;
            accelPub.publish(Vector3StampedMsg);
            //ROS_INFO("Debug Joystick: % 1.6f  % 1.6f % 1.6f ", Vector3StampedMsg.vector.x, Vector3StampedMsg.vector.y, Vector3StampedMsg.vector.z);
            //G

            //Send Fcx manual
            std_msgs::Float64 throttleMsg;
            throttleMsg.data = joy->axes[1];// ROS_INFO("throttle received: [%f]", throttleMsg.data);
            throttlePub.publish(throttleMsg);

            //Send _vzr for depth control
            geometry_msgs::PoseStamped PoseStampedMsg2;
            PoseStampedMsg2.pose.orientation.x = joy->buttons[4];  //LB
            PoseStampedMsg2.pose.orientation.y = joy->buttons[5];  //RB
            quatPub.publish(PoseStampedMsg2);

            //Adjust zero pressure
            geometry_msgs::PoseStamped PoseStampedMsg;
            if (_PressureButtonStatus > 0){
                PoseStampedMsg.pose.position.z = 1.0;
            }else if (_PressureButtonStatus < 0){
                PoseStampedMsg.pose.position.z = -1.0;
            }else{
                PoseStampedMsg.pose.position.z = 0.0;
            }
            positionPub.publish(PoseStampedMsg);


        } else {
            //In Autonomous control mode with Homography calculation has been launched
            geometry_msgs::TwistStamped TwistStampedMsg;

            TwistStampedMsg.twist.angular.y = 0.0;
            if (_LampButtonStatus > 0) {
                TwistStampedMsg.twist.angular.z = 1.0;
            }else if (_LampButtonStatus < 0)
            {
                TwistStampedMsg.twist.angular.z = -1.0;
            }else{
                TwistStampedMsg.twist.angular.z = 0.0;
            }
            ROS_INFO("Debug Lamp in Autonomous: % 1.6f  ", TwistStampedMsg.twist.angular.z);
            angVelPub.publish(TwistStampedMsg);

            //Send Fcx manual  - for not stuck in emmergency lhnguyen debug
            std_msgs::Float64 throttleMsg;
            throttleMsg.data = joy->axes[1];// ROS_INFO("throttle received: [%f]", throttleMsg.data);
            throttlePub.publish(throttleMsg);
        }
    }
}

void ControlInROS::process(){

    if (!_isManualMode) {

        if (!_isStartedIntegrator) {
            _z_ep (0) = 0.0;
            _z_ep (1) = 0.0;
            _z_ep (2) = 0.0;
            //(0.0, 0.0, 0.0);
            _isStartedIntegrator = true;
        }

        float h11 = _h11; float h12 =  _h12;    float h13 = _h13;
        float h21 = _h21; float h22 =  _h22;    float h23 = _h23;
        float h31 = _h31; float h32 =  _h32;    float h33 = _h33;
        ROS_INFO("imghom row1: % 1.6f % 1.6f % 1.6f ", _h11, _h12, _h13 );
        ROS_INFO("imghom row2: % 1.6f % 1.6f % 1.6f ", _h21, _h22, _h23 );
        ROS_INFO("imghom row3: % 1.6f % 1.6f % 1.6f ", _h31, _h32, _h33 );

        //Angular velocites ratex, ratey, ratez

        //Mat H = Mat::eye(3, 3, CV_32F);

        Matrix3f H;
        H << h11, h12, h13,
             h21, h22, h23,
             h31, h32, h33;

        float ratex_average = 0.0;
        float ratey_average = 0.0;
        float ratez_average = 0.0;

        //Calculate the average value of angular velocity measured by gyro after each 10 reads
        for (int i=0; i < 10; i++){
            //Prevent too big values of angular velocity components in initilization
            //This can happen because spinOnce (for gyroCallback()) runs lately after process(), right after starting
            //AUV can not rotate with an angular velocity > 100 rad/s!!
            if (abs(ratex_vector_new[i]) > 100.0) ratex_vector_new[i] = 0.0;
            if (abs(ratey_vector_new[i]) > 100.0) ratey_vector_new[i] = 0.0;
            if (abs(ratez_vector_new[i]) > 100.0) ratez_vector_new[i] = 0.0;

            ratex_average += ratex_vector_new[i];
            ratey_average += ratey_vector_new[i];
            ratez_average += ratez_vector_new[i];
        }
        ratex_average = ratex_average/10.0; //5.0;
        ratey_average = ratey_average/10.0; //5.0;
        ratez_average = ratez_average/10.0; //5.0;



        Vector3f Angular_vel(0.0, 0.0, 0.0);
        Angular_vel << ratex_average, ratey_average, ratez_average;

        //ROS_INFO("Debug average angVelo: % 1.6f % 1.6f % 1.6f ", ratex_average, ratey_average, ratez_average);

        Matrix3f I33;
        I33.setIdentity();
        //I33 << 1.0, 0.0, 0.0,  0.0, 1.0, 0.0,  0.0, 0.0, 1.0;

        Vector3f m_star(0.0, 0.0, 1.0);
        Vector3f n_star(0.0, 0.0, 1.0);
        float a_star = 0.0;
        //a_star = n_star.transpose()*m_star/d_star;
        a_star = 1.0/d_star;

        float K1 = 3.0*s;
        float k2 = 8.0/3.0 * s*s / a_star;
        float k3 = 1.0/3.0 * s*s / a_star;



        Vector3f ep(0.0, 0.0, 0.0);
        ep = (I33 - H)*m_star;
        Vector3f Fc_bar(0.0, 0.0, 0.0);
        DiagonalMatrix<float, 3> M(M11, M22, M33);   // Estimated mass matrix
        float m_bar = M22*M22;


        //Manually adjust Gravity-Boyancy force
        //Use the same button used for adjusting pressure_zero_level in manual control mode
        //



        //Fgb_3_rate = 0.1*Fgb_3_rate;
        for (int i=0; i < 10; i++){
            float Fgb_3_rate = 0.0;
            if (_PressureButtonStatus > 0){
                Fgb_3_rate = 1.0;
            }else if (_PressureButtonStatus < 0){
                Fgb_3_rate = -1.0;
            }else{
                Fgb_3_rate = 0.0;
            }
            _Fgb_3 += Fgb_3_rate*dt;
        }


        //Vector3f Fgb(0.0, 0.0, 1.0*2.5);
        Vector3f Fgb(0.0, 0.0, _Fgb_3);
        ROS_INFO("Debug force gravity-boyance: % 1.6f ", _Fgb_3);




        //bool useIntegrator = false;
        bool useIntegrator = true;

        if (!useIntegrator){


            for (int i=0; i < 10; i++){
                 Vector3f ep_hat_dot = -Angular_vel.cross(_ep_hat) - _ep_hat*K1 + ep*K1;
                 _ep_hat += ep_hat_dot*dt;  //Be careful, check if it becomes to big!!!
            }

            _ep_hat = sat3Function(_ep_hat, 15.0f);



            Vector3f ep_tilde   =  _ep_hat*1.0 - ep;
            ROS_INFO("Debug force Ep: % 1.6f % 1.6f % 1.6f ", ep[0], ep[1], ep[2]);



            //float eta_1 = 1.8;
            //float eta_2 = 2.3;

            Vector3f temp = sat3Function(ep_tilde*k2, eta_1)  - sat3Function(ep*k3, eta_2);

            Fc_bar = M.inverse()*temp*m_bar - Fgb;

        }else{

            Vector3f ep_bar_new(0.0, 0.0, 0.0);

            for (int i=0; i<10; i++){

                Vector3f z_ep_dot = - Angular_vel.cross(_z_ep) + ep;

                //float dt = 0.005;
                //float kI = 0.7;
                //float K1 = 3s;

                _z_ep += z_ep_dot*dt;
                _z_ep = sat3Function(_z_ep, 2.0f*10.0f);//For safety


                Vector3f ep_bar = ep + _z_ep*kI;

                Vector3f ep_hat_dot = -Angular_vel.cross(_ep_hat) - _ep_hat*K1 + ep_bar*K1;
                _ep_hat += ep_hat_dot*dt;  //Be careful, check if it becomes to big!!!
                _ep_hat = sat3Function(_ep_hat, 20.0f);

                ep_bar_new = ep_bar;
            }

                //Vector3f Fgb(0.0, 0.0, 0.0);
                //float K2 = ...;
                //float K3 = ...;

                //Matrix3f M; // Estimated mass matrix
                //M << 1.5*1.39*0.8 ,            0.0            ,       0.0    ,ep_bar
                //        0.0       ,       1.5*4.26*1.2        ,       0.0    ,
                //        0.0       ,            0.0            ,  1.5*4.02*0.8;


                //float eta_1 = 1.8;
                //float eta_2 = 2.3;

                Vector3f temp = sat3Function((_ep_hat - ep_bar_new)*k2, eta_1)  - sat3Function(ep_bar_new*k3, eta_2);

                Fc_bar = M.inverse()*temp*m_bar - Fgb;  //Be careful about M.inverse(), can cause processor overloading
        }

	geometry_msgs::Vector3Stamped zepMsg;
        zepMsg.header.stamp = ros::Time::now();
        zepMsg.vector.x = _z_ep[0]; //0.55; //
        zepMsg.vector.y = _z_ep[1]; //0.66; //
        zepMsg.vector.z = _z_ep[2]; //0.77;
        zepPub.publish(zepMsg);


        geometry_msgs::Vector3Stamped epMsg;
        epMsg.header.stamp = ros::Time::now();
        epMsg.vector.x = ep[0]; //0.55; //
        epMsg.vector.y = ep[1]; //0.66; //
        epMsg.vector.z = ep[2]; //0.77;
        epPub.publish(epMsg);

        geometry_msgs::Vector3Stamped epHatMsg;
        epHatMsg.header.stamp = ros::Time::now();
        epHatMsg.vector.x = _ep_hat[0]; //0.55; //
        epHatMsg.vector.y = _ep_hat[1]; //0.66; //
        epHatMsg.vector.z = _ep_hat[2]; //0.77;
        epHatPub.publish(epHatMsg);

        geometry_msgs::Vector3Stamped FcMsg;
        FcMsg.header.stamp = ros::Time::now();
        FcMsg.vector.x = Fc_bar[0]; //0.55; //
        FcMsg.vector.y = Fc_bar[1]; //0.66; //
        FcMsg.vector.z = Fc_bar[2]; //0.77;
        FcontrolPub.publish(FcMsg);



        ROS_INFO("Debug force Fc: % 1.6f % 1.6f % 1.6f ", Fc_bar[0], Fc_bar[1], Fc_bar[2]);




        //float k_theta_2 = 0.0;
        //float k_theta_1 = 0.0;
        //float Delta_theta  = 1.0;


        float omega_3r_dot(0.0);
        for (int i=0; i < 10; i++){
            omega_3r_dot = -k_theta_2*_omega_3r - k_theta_1* satFunction(h12,Delta_theta);
            _omega_3r += omega_3r_dot*dt;  //Be careful, check if it becomes to big!!!
            _omega_3r = satFunction(_omega_3r, 2.0f);



        //_omega_3r = -h12;
        if (i = 9){
           // ROS_INFO("DebugAAA h12  _omega_3r: % 1.6f % 1.6f ", h12, _omega_3r);
        }

        }

        ROS_INFO("Debug h12  _omega_3r omega_3r_dot: % 1.6f % 1.6f %1.6f", h12, _omega_3r, omega_3r_dot);

        Matrix3f e3_cross;
        e3_cross << 0.0, -1.0, 0.0,
                    1.0,  0.0, 0.0,
                    0.0,  0.0, 0.0;

        Vector3f e3_vector(0.0, 0.0, 1.0);
        Vector3f rc(rc_x, rc_y, rc_z);  //Position of the camera
        //ROS_INFO("Debug gains: % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f ", kI, K1, k2, k3, m_bar, eta_1, eta_2, Delta_theta );

        //Vector3f Fc = Fc_bar + (I33*omega_3r_dot + e3_cross*_omega_3r*_omega_3r)*M*(e3_vector.cross(rc));// +

        Vector3f Fc = Fc_bar - (I33*omega_3r_dot + e3_cross*_omega_3r*_omega_3r)*M*(e3_vector.cross(rc));
        Fc = sat3Function(Fc, 6.0);

        //ROS_INFO("Debug force Fc: % 1.6f % 1.6f % 1.6f ", Fc[0], Fc[1], Fc[2]);

        //Can nhap cac he so tu .conf
        //Can xac dinh lai Fgb,
        //Co can tinh dt theo thoi gian thuc? hay dat hang so = 5ms???

        //ROS_INFO("Debug gains: % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f ", kI, K1, k2, k3, m_bar, eta_1, eta_2, Delta_theta );
        //ROS_INFO("Debug 2 angular velocity: % 1.6f % 1.6f % 1.6f ", ratex, ratey, ratez);


        //publish Fc_bar , _omega_3r and omega_3r_dot

        geometry_msgs::Vector3Stamped Vector3StampedMsg;
        Vector3StampedMsg.vector.x = Fc[0]; //0.55; //
        Vector3StampedMsg.vector.y = Fc[1]; //0.66; //
        Vector3StampedMsg.vector.z = Fc[2]; //0.77;
        accelPub.publish(Vector3StampedMsg);

        geometry_msgs::PoseStamped PoseStampedMsg;
        PoseStampedMsg.pose.position.x =  _omega_3r;//    0.88;
        PoseStampedMsg.pose.position.y =  omega_3r_dot;// 0.99;
        //PoseStampedMsg.pose.position.z = joy->axes[0];
        positionPub.publish(PoseStampedMsg);

    }
}



void ControlInROS::spin(){
	ros::Rate r(10);   //20Hz
	while (ros::ok ())
	{

		process();
		ros::spinOnce();
		r.sleep();
		
	}

}


void ControlInROS::loadparameters(){
	Config conf;
	try {
        //conf.readFile("/home/lhnguyen/catkin_ws/src/control_in_ros/config/Homog_control.conf");
	conf.readFile("/home/lhnguyen/catkin_ws/src/outerloop_control_In_ROS/config/Homog_control.conf");

	}catch(ParseException& e){

		std::cerr << "Parse exception when reading control.conf";
		std::cerr << "line" << e.getLine() << "error:" << e.getError() << std::endl;
	}

	bool collectDebug = false;

	try{
		collectDebug =  conf.lookup("control_data");

		//Outloop_gains
		he_so_1 = conf.lookup("control.outloop_gains.he_so_1");

		dt          = conf.lookup("control.outloop_gains.dt");
		s           = conf.lookup("control.outloop_gains.s");	
		d_star      = conf.lookup("control.outloop_gains.d_star");
		kI          = conf.lookup("control.outloop_gains.kI");
		M11         = conf.lookup("control.outloop_gains.M11");
		M22         = conf.lookup("control.outloop_gains.M22");
		M33         = conf.lookup("control.outloop_gains.M33");
		eta_1       = conf.lookup("control.outloop_gains.eta_1");
		eta_2       = conf.lookup("control.outloop_gains.eta_2");
		k_theta_1   = conf.lookup("control.outloop_gains.k_theta_1");
		k_theta_2   = conf.lookup("control.outloop_gains.k_theta_2");
		Delta_theta = conf.lookup("control.outloop_gains.Delta_theta");

		rc_x        = conf.lookup("control.outloop_gains.rc_x");
		rc_y        = conf.lookup("control.outloop_gains.rc_y");
		rc_z        = conf.lookup("control.outloop_gains.rc_z");




	}catch(SettingException& e){

		std::cerr << "Setting exception" << std::endl;
		std::cerr << "path" << e.getPath() << "what:" << e.what() << std::endl;

	}catch(ConfigException& e){

		std::cerr << "Config exception" << std::endl;
		std::cerr << "what:" << e.what() << std::endl;

	}

	cout << "Loaded Parameters " << he_so_1 <<endl;      //Printing without timestamp
	//ROS_INFO("Debug load parameters: % 1.6f ", he_so_1); //Printing with timestamp

	//ROS_INFO("Debug: % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f % 1.6f", dt, s, d_star, kI, M11, M22, M33, eta_1, eta_2, k_theta_1, k_theta_2, Delta_theta, rc_x, rc_y, rc_z );
}

float ControlInROS::satFunction(float x, float delta_x)
{
  if (x > delta_x) {
    return delta_x;
  }
  else if (x < -delta_x){
    return -delta_x;
  }
  else{
    return x;
  }

}

Vector3f ControlInROS::sat3Function(Vector3f x, float delta_x)
{//(unsigned i = 0; i < 6; i++)
  //for (int i=0; i < 3; ++i){
  for (unsigned i = 0; i < 3; i++){
    if (x(i) > delta_x) {
       x(i) = delta_x;
    }
    else if (x(i) < -delta_x){
      x(i) = -delta_x;
    }
  } 
  return Vector3f (x(0), x(1), x(2));
}

