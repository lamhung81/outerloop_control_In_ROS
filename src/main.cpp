#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>

#include <sstream>
#include <string>
#include <time.h>
#include <algorithm>
#include <ros/ros.h>
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
//#include <sensor_msgs/image_encodings.h>
//#include <sensor_msgs/Imu.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
#include <opencv2/highgui/highgui.hpp>
#include <rosbag/bag.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64MultiArray.h>
//#include "algorithmhom.h"
//#include <thread>  //c++ 11 ?

#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/Float64.h>

#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>


#include <libconfig.h++>
#include <libconfig.h>

#include "ControlInROS.h"

using namespace std;
using namespace message_filters;
//using namespace sensor_msgs;






int main(int argc, char** argv){
    ros::init(argc, argv, "control_in_ros_node");
    ControlInROS control_in_ros_node;

    control_in_ros_node.loadparameters();
	//control_in_ros_node.process();
    control_in_ros_node.spin();
	
	//ros::spin();
	return 0;
}


/*
int main(int argc, char** argv){

	ros::init(argc, argv, "image_listener");
	algorithmhom hom;
	//hom.process();
	//hom.loadparameters();
	//hom.spin();

	return 0;
}
*/
