#ifndef SRC_CONTROLINROS_H_
#define SRC_CONTROLINROS_H_

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>

#include <sstream>
#include <string>
#include <time.h>
#include <algorithm>
#include <ros/ros.h>
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
//#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/Imu.h>
#include <message_filters/subscriber.h>
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/sync_policies/approximate_time.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/CameraInfo.h>
#include <opencv2/highgui/highgui.hpp>
#include <rosbag/bag.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/Float64MultiArray.h>
//#include "algorithmhom.h"
//#include <thread>  //c++ 11 ?

#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include <std_msgs/Float64.h>

#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Vector3Stamped.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>

#include <libconfig.h++>
#include <libconfig.h>
#include <../eigen3/Eigen/Core>
#include <../eigen3/Eigen/Dense>

#include "hom.h"


using namespace std;
using namespace message_filters;

using namespace cv;
using namespace libconfig;
using namespace sensor_msgs;
using namespace Eigen;


class ControlInROS
{
	public:
      ControlInROS();
	  void joyCallback(const sensor_msgs::Joy::ConstPtr &joy);
	  void gyroCallback(const sensor_msgs::Imu::ConstPtr &gyrodata);
      //void homoCallback(const sensor_msgs::Imu::ConstPtr &homodata);
      void homoCallback(const i3s_img_proc::hom::ConstPtr &homodata);
	  void process();

	  virtual void spin();

	  virtual void loadparameters();

	  float satFunction(float x, float delta_x);
	  Vector3f sat3Function(Vector3f x, float delta_x);
	  

	private:
	  ros::NodeHandle nh;		

	  ros::Subscriber joySub;
	  ros::Subscriber gyroSub;
      ros::Subscriber homoSub;
	  
	  //For publishing Throttle, yaw, and e3 orientation
	  ros::Publisher angVelPub;
	  ros::Publisher throttlePub;
	  ros::Publisher quatPub;

	  //For publishing Fc_bar, _omega_3r and omega_3r_dot
	  ros::Publisher accelPub;
	  ros::Publisher positionPub;
	  ros::Publisher velPub;
      
      ros::Publisher zepPub;
      ros::Publisher epPub;
      ros::Publisher epHatPub;
      ros::Publisher FcontrolPub;



	  //Timestamps
	  long double gyrotimestamp;

	  //Gyro data
	  float ratex;
	  float ratey;
	  float ratez;

      float _h11;
      float _h12;
      float _h13;
      float _h21;
      float _h22;
      float _h23;
      float _h31;
      float _h32;
      float _h33;

	  Vector3f _z_ep;
	  Vector3f _ep_hat;

	  float _omega_3r;

	  float he_so_1;


	  float dt ; //= 0.005; //Integration step    5ms

	  float	s ; //= 1.4142;  //Pole

	  float	d_star ; //= 2.0 ; // Original form AUV's CB to the site

	  float kI ; //= 0.7; 
		
		//Estimated mass matrix
	  float M11 ; //= 1.5*1.39*0.8;
	  float M22 ; //= 1.5*4.26*1.2;
	  float M33 ; //= 1.5*4.02*0.8;

	  float eta_1 ; //= 1.8;
	  float eta_2 ; //= 2.3;

	  float k_theta_2 ; //= 0.5;
	  float k_theta_1 ; //= 1.4142;

	  float Delta_theta  ; //= 1.0;

		//Camera's position
	  float rc_x ; //= 0.05;
	  float rc_y ; //= 0.0; 
          float rc_z ; //= 0.15; 
	  
};
#endif /* SRC_CONTROLINROS_H_ */
