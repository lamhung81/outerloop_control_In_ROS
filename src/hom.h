// Generated by gencpp from file i3s_img_proc/hom.msg
// DO NOT EDIT!


#ifndef I3S_IMG_PROC_MESSAGE_HOM_H
#define I3S_IMG_PROC_MESSAGE_HOM_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace i3s_img_proc
{
template <class ContainerAllocator>
struct hom_
{
  typedef hom_<ContainerAllocator> Type;

  hom_()
    : header()
    , homography()  {
      homography.assign(0.0);
  }
  hom_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , homography()  {
  (void)_alloc;
      homography.assign(0.0);
  }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef boost::array<double, 9>  _homography_type;
  _homography_type homography;





  typedef boost::shared_ptr< ::i3s_img_proc::hom_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::i3s_img_proc::hom_<ContainerAllocator> const> ConstPtr;

}; // struct hom_

typedef ::i3s_img_proc::hom_<std::allocator<void> > hom;

typedef boost::shared_ptr< ::i3s_img_proc::hom > homPtr;
typedef boost::shared_ptr< ::i3s_img_proc::hom const> homConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::i3s_img_proc::hom_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::i3s_img_proc::hom_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace i3s_img_proc

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg'], 'i3s_img_proc': ['/home/ninad/catkin_ws/src/i3s_img_proc/msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::i3s_img_proc::hom_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::i3s_img_proc::hom_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::i3s_img_proc::hom_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::i3s_img_proc::hom_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::i3s_img_proc::hom_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::i3s_img_proc::hom_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::i3s_img_proc::hom_<ContainerAllocator> >
{
  static const char* value()
  {
    return "ffdba792548edbfb1333894295680586";
  }

  static const char* value(const ::i3s_img_proc::hom_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xffdba792548edbfbULL;
  static const uint64_t static_value2 = 0x1333894295680586ULL;
};

template<class ContainerAllocator>
struct DataType< ::i3s_img_proc::hom_<ContainerAllocator> >
{
  static const char* value()
  {
    return "i3s_img_proc/hom";
  }

  static const char* value(const ::i3s_img_proc::hom_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::i3s_img_proc::hom_<ContainerAllocator> >
{
  static const char* value()
  {
    return "Header header\n\
float64[9] homography\n\
\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::i3s_img_proc::hom_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::i3s_img_proc::hom_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.homography);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct hom_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::i3s_img_proc::hom_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::i3s_img_proc::hom_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "homography[]" << std::endl;
    for (size_t i = 0; i < v.homography.size(); ++i)
    {
      s << indent << "  homography[" << i << "]: ";
      Printer<double>::stream(s, indent + "  ", v.homography[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // I3S_IMG_PROC_MESSAGE_HOM_H
